import api from "../configuration/api";
const BASE_URI = '/persons';

export async function findAllPersons() {
    return api
        .get(`${BASE_URI}`)
        .then((response) => {
            return response.data;
        })
}

export async function save(person) {
    return api.post(`${BASE_URI}`, person)
        .then(response => {
            return response
        })
}