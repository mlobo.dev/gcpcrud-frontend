import React, { useEffect, useState } from 'react';
import * as personService from '../services/PersonService';
import './home.css';

export default function Home() {

    const [persons, setPersons] = useState([])
    const [name, setName] = useState();

    useEffect(() => {
        findPersons();
    }, [persons]);

    async function findPersons() {
        const result = await personService.findAllPersons();
        console.log('result', result)
        setPersons(result);
    }

    async function savePerson(person) {
        personService.save(person);
        setName("")
        personService.findAllPersons();
    }

    return (
        <>
            <div className='input-area'>
                <input
                    className='input-name'
                    placeholder='Put your name here'
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                ></input>
                <button
                    onClick={() => savePerson({ name: name })}
                    className='save-buttom'
                >send</button>
            </div>

            <div>
                <ul>
                    {persons.map((p, i) => {
                        return (

                            <li key={i}>{p.name}</li>

                        )
                    })}
                </ul>

            </div>
        </>
    )
}
